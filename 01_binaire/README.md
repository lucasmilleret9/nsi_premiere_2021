## Représentation des nombres en binaire

### Présentation

Un ordinateur n'utilise pas les nombres comme nous les utilisons directement. Il utilise des nombres codés en base 2 (ou binaire) ce qui correspond au contact ou au non contact d'un transistor. Il effectue ensuite une conversion pour que l'on puisse manipuler les nombres tels que nous les utilisons.
Voyons comment ces conversions sont effectuées.

### Pour les nombres entiers positifs

https://pixees.fr/informatiquelycee/n_site/nsi_prem_base_2_16.html

### Pour les nombres entiers relatifs (positif ou négatifs)

https://pixees.fr/informatiquelycee/n_site/nsi_prem_comp_2.html

<img src='img/complement_2.png'>

### Pour les nombres décimaux ( à virgule ..)

https://pixees.fr/informatiquelycee/n_site/nsi_prem_float.html
