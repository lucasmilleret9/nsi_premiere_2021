## Introduction réseaux

https://pixees.fr/informatiquelycee/n_site/nsi_prem_intro_reseau.html

## Protocoles TCP/IP

https://pixees.fr/informatiquelycee/n_site/nsi_prem_tcpip.html

## Modèle TCP/IP

https://pixees.fr/informatiquelycee/n_site/nsi_prem_modele_tcpip.html

## Protocole Bit alterné

https://pixees.fr/informatiquelycee/n_site/nsi_prem_bit_alt.html

## Exercices Filius 

https://pixees.fr/informatiquelycee/n_site/nsi_prem_simReseau1.html

https://pixees.fr/informatiquelycee/n_site/snt_internet_sim1.html
