from flask import Flask, render_template
#import json

app = Flask(__name__)


@app.route('/index.html', methods=['GET'])
def accueil():
    return render_template('index.html')

@app.route('/bonjour/<nom>', methods=['GET'])
def bonjour(nom):
    return render_template('bonjour.html', cle_nom=nom)

@app.route('/aurevoir/<nom>', methods=['GET'])
def aurevoir(nom):
    return render_template('aurevoir.html', cle_nom=nom)

prenoms = ['Pierre', 'Paul', 'Jacques','Toto']

@app.route('/liste', methods=['GET'])
def liste():
    return render_template('boucle_for.html', prenoms=prenoms)

@app.route('/parite/<nombre>', methods=['GET'])
def if_else(nombre):
    if int(nombre) % 2 == 0: # test de parité
        pair = True
    else:
        pair = False
    return render_template('if_else.html', nombre=nombre, pair=pair)


app.run(host='0.0.0.0', port='5001', debug=True)