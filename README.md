# Première NSI 2021

Ceci est le projet contenant le programme de cette année que je complèterai au fur et à mesure de l'année.

Pour suivre les tutos d'installation, il suffit de naviguer dans le dossier __00_installation__, puis dans le dossier voulu et de suivre les instructions.

Des fautes de frappe ont pu se glisser. Merci de me les signaler afin que je puisse les corriger.
